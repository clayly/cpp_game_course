#!/usr/bin/env bash

EXE=hello_exec
RETURN_CODE=1

if [ ! -f $EXE ]; then
    echo "File not found"
else
    ./$EXE
    RETURN_CODE=$?
    if [ $RETURN_CODE -eq 0 ]; then
        echo "Use good"
    else
        echo "Error"
    fi
fi

exit $RETURN_CODE

