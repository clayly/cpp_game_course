#include <fstream>
#include <memory>
#include <vector>
#include <iostream>

void printFail(const std::ifstream &toPrint) {
    std::cerr <<
              "s_goodBit:" << std::ifstream::goodbit <<
              " s_failbit:" << std::ifstream::failbit <<
              " s_badbit:" << std::ifstream::badbit <<
              " s_eofbit:" << std::ifstream::eofbit <<
              std::endl;
    std::cerr <<
              "good:" << toPrint.good() <<
              " rdstate:" << toPrint.rdstate() <<
              " fail:" << toPrint.fail() <<
              " bad:" << toPrint.bad() <<
              " eof:" << toPrint.eof() <<
              std::endl;
}

int main(int argc, char **argv) {
    using namespace std;
    ifstream file("res/text-utf8-linux.txt", ios::in);
    auto data = new vector<string>();
    if (file.eof()) {
        printFail(file);
        throw runtime_error("file.eof() on open");
    }
    if (file.fail()) {
        printFail(file);
        throw runtime_error("file.fail() on open");
    }
    if (!file.is_open()) {
        printFail(file);
        throw runtime_error("!file.is_open() on open");
    }
    file.seekg(0, ios::beg);
    string line;
    while (true) {
        getline(file, line);
        if (file.fail() && !file.eof()) {
            printFail(file);
            file.close();
            throw runtime_error("file.fail() on read");
        }
        data->push_back(line);
        if (file.eof()) {
            break;
        }
    }
    for (auto const &str : *data) {
        cout << str << endl;
    }
    file.close();
}