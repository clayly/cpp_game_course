# Static and dynamic linking

Project subfolders:

* *hello_bin*: program which dependent on library;
* *hello_lib*: library which can be used in few ways:
    * included header-only.
    One hxx-file.
    * included as sources.
    Two files:
        * cxx-file for implementation;
        * hxx-file for headers (declaration);
    * included as static library;
    * included as dynamic library.
    
- - -

## Structure convensions

Folders:

* *build*: for generated code.
Excluded from git;
* *src*: code (implementation) itself;
* *include*: something for #define include.
Example: headers.

- - -

## From source to executable

### Base

Parts of g++ toolchain in the execution order:

- - -

#### **1. cpp**

Preprocessor.  
Processes all defines etc.  
Example of usage:  
`cpp -I../../hello_lib/include ../src/main.cxx -o main.ii`  
Example of usage within g++:  
`g++ -E -I../../hello_lib/include ../src/main.cxx -o main.ii`

- - -

#### **2. as**

Have few names:

* translator to assembly-language;
* assembler (not very strange one);
* compiler (strange one).

Translates C++ code to assembly-language.  
Example of usage: have a lot of specific flags to set, so its better to call it from g++.  
Example of usage within g++:  
`g++ -S -std=c++17 -I../../hello_lib/include/ ../src/main.cxx -o hello.s`

- - -

#### **3. g++**

Toolchain itself, but also compiler, because there is no separate tool for it.  
Have few names:

* compiler;
* assembler (strange one).

Compiles assembly-language code to object files.  
Example of usage as compiler:  
`g++ -c -std=c++17 -I../../hello_lib/include/ ../src/main.cxx -o hello.o`

- - -

#### **4. ld**

Linker.  
Example of usage: have a lot of specific flags to set, so its better to call it from g++.  
Example of usage within g++:  
`g++ ../../hello_lib/build/hello_lib.o hello.o -o hello`

- - -

### Toolchain

Example of usage g++ as a toolchain:  
`g++ -std=c++17 -I../../hello_lib/include ../../hello_lib/src/hello_lib.cxx ../src/main.cxx -o hello`

Verbose output flag `-v` with g++ can be used if you want to try build your executable step by step because its provides flags, variables and pathes for other tools in toolshain.

- - -

### Header-only or source

Our library is not header-only, so we can use it only as source (dynamically linked dependencies inside our library are staying here):  
`g++ -std=c++17 -I../../hello_lib/include ../../hello_lib/src/hello_lib.cxx ../src/main.cxx -o hello`

But if it were header-only, we can point only to inlcude-folder:  
`g++ -std=c++17 -I../../hello_lib/include ../src/main.cxx -o hello`

Check result for dependencies:

````
[xxx@localhost build]$ ldd hello 
    linux-vdso.so.1 (0x00007ffef8ff2000)
    libstdc++.so.6 =&gt; /lib64/libstdc++.so.6 (0x00007f9bdb583000)
    libm.so.6 =&gt; /lib64/libm.so.6 (0x00007f9bdb1ef000)
    libgcc_s.so.1 =&gt; /lib64/libgcc_s.so.1 (0x00007f9bdafd7000)
    libc.so.6 =&gt; /lib64/libc.so.6 (0x00007f9bdac18000)
    /lib64/ld-linux-x86-64.so.2 (0x00007f9bdb915000)
````

If we will add `-static` flag, g++ will try to link static as much as he can:
`g++ -std=c++17 -I../../hello_lib/include ../../hello_lib/src/hello_lib.cxx ../src/main.cxx -o hello`

If we have already installed static versions of all needed libraries, there will empty list.
Check result for dependencies:

````
[xxx@localhost build]$ ldd hello 
    not a dynamic executable
````

- - -

### Static

- - -

### Dynamic

- - -