# Libraries

- - -

## Static

System library folders:

* `/usr/lib`;
* `/usr/local/lib`;
* `/lib`;
* `lib32` or `lib64` as well.

Archivate object file to static library:  
`ar rus(may ne other options) libhello_lib.a hello_lib.0`

If library is on `/usr/lib`, it can be linked as:  
`g++ -std=c++17 ../src/main.cxx -lhello_lib`  
Else:  
`g++ -std=c++17 -L<PATH_TO_LIB_DIR> ../src/main.cxx -lhello_lib`

Header `hello_lib.hxx` to `usr/include`,
so it can be accessed without `-I<INCLUDE_PATH>` flag
with `#include <hello_lib.hxx>`.

Static versions of most common libraries:

* `glibc-static`.

- - -

## Shared

Must be used flag`-fPIC`.
Generated assembly will have relative offsets instead of absolute
so if you will load it in other environment it will work anyway.

`g++ -std=c++17 -shared -fPIC -o libhello_lib.so ../src/hello_lib.cxx`
`g++ -std=c++17 ../src/main.cxx -I../../hello_lib/include -L../../hello_lib/build -lhello_lib`

It will not run, if shared lib is not in system folder, but there is some tricks:

1. `export LD_LIBRARY_PATH=/<path_to_shared_lib_dir>`
1. `LD_PRELOAD=$PWD/libhello_lib.so ./hello`

`$PWD` is for current folder.  

For some reason, if you preload with LD_PRELOAD, even the same library with same
name but from other place will be not recognized.

- - -

## Common

**g++** always prefer dynamic linking.

`-static` flag will lead to an error if there no static version of any library.

Use dynamic version of hello_lib:  
`g++ -std=c++17 -o hello ../src/main.cxx -Wl,-Bdynamic -lhello_lib`

Drop any unused dynamic library:  
`-Wl,--as-needed`

Use static version of hello_lib and dynamic version of SOME_LIB:  
`g++ -std=c++17 -o hello ../src/main.cxx -Wl,-Bstatic -lhello_lib -Wl,Bdynamic -l<SOME_LIB>`

Directly point to g++ which library i want to link with (full name needed):  
`g++ -std=c++17 -o hello ../src/main.cxx -l:libhello_lib.a`

Only one is static, everything else dynamic:  
`g++ -std=c++17 -o hello ../src/main.cxx -static -Wl,-Bstatic -lhello_lib -Wl,-Bdynamic`

- - -

## Additional

Windows object files (format Portable Executable):

* .obj;
* .dll;
* .sys;
* .ocx;
* .exe.

Libraries:

* shared:
    * .dll (win);
    * .so (lin);
* static:
    * .a (lin);
    * .lib (win).

Command tools for exploring object files:

* **objdump**;
* **nm**. 

- - -
