= Common

== PowerShell

NOTE: 'string' and "string" are the same.

NOTE: Uppercase and lowercase are the same.

Settings file: `C:\Users\<user>\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1`. +
Commands froim settings file are loaded before PowerShell started.

Set all colors in console to black. +
Yoc can put it to settings file.
[source]
----
Get-PSReadLineOption // read current settings
Set-PSReadLineOption -colors @{
    Command            = 'Black'
    Comment            = 'Black'
    ContinuationPrompt = 'Black'
    DefaultToken       = 'Black'
    Emphasis           = 'Black'
    Error              = 'Black'
    Keyword            = 'Black'
    Member             = 'Black'
    Number             = 'Black'
    Operator           = 'Black'
    Parameter          = 'Black'
    Selection          = 'Black'
    String             = 'Black'
    Type               = 'Black'
    Variable           = 'Black'
}
----

== ssh

Docs: https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_overview.

Key permissions: https://superuser.com/questions/1296024/windows-ssh-permissions-for-private-key-are-too-open. +
Need to remove all users and groups except owner for key.

[source]
----
Get-WindowsCapability -Online | ? Name -like 'OpenSSH*'
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

Start-Service sshd
Set-Service -Name sshd -StartupType 'Automatic'
Get-NetFirewallRule -Name *ssh*
----

== Useful commands

Environment variables.
[source]
----
$Env:<name> // print or use var
$Env:<name> = "new value" // change var
Get-Item Env:<name> // print or use var
Set-Item Env:<name> "new_value" // change var
----

Rename something.
[source]
----
Rename-Item <old_name> <new_name>
----

Get path to executable "command_name".
[source]
----
Get-Command <command_name>
----

Search for entity with such name.
[source]
----
Get-ChildItem -Path <folder> -File/*only files*/ -Recurse/*recursive*/ -Include <regexp>/*only if matches*/ // only in folder
| Format-Table -Property <property>/*Name, FullName, etc.*/ // format output
| Select-Object FullName // looks like previous
| ForEach-Object { adb install -g $_.FullName } // perform action
| Foreach-Object { Copy-Item $_.FullName -Destination <folder> } // copy to folder
| ForEach-Object { Set-Location -Path $_.Directory.Fullname; // set location to file's location
                   $Command = "$_ arguments"; // set up command to execute file with arguments
                   Invoke-Expression $Command; // execute command
                   Set-Location -Path <folder> } // and return
----

Search for running process with name which includes this word.
[source]
----
Get-Process | Where-Object {$_.Name -like <regexp>}
----

Set permissions for executing scripts. +
Only for Administrator-level PowerShell. +
`Unrestricted` is enough for python scripts.
[source]
----
Set-ExecutionPolicy Unrestricted
----

== Remote PowerShell

Check version of PowerShell. +
Available commands and options different from version to version.
[source]
----
$PSVersionTable.PSVersion
----

Check network adapters.
[source]
----
Get-NetConnectionProfile
----

Set profile of network adapter to "Private".
[source]
----
Set-NetConnectionProfile -InterfaceIndex <number> -NetworkCategory <category>/*Private, Public*/
----

Trusted hosts to which we can connect.
[source]
----
Get-Item wsman:localhost\Client\TrustedHosts // check
Set-Item wsman:localhost\Client\TrustedHosts -value * // set
----

Enable remote connections and control.
[source]
----
Enable-PSRemoting -SkipNetworkProfileCheck/*if dont wont to set rules for network adapter*/
----

Connect to shell of PC with name "pc_name" (can be "localhost").
[source]
----
Enter-PSSession <pc_name>
----

Will ask for user password.
[source]
----
Enter-PSSession -ComputerName by14 -Authentication Default -Credential TECONPC\Rutsky
----

Iterate through directories and pull if there is git repo.
[source]
----
Get-ChildItem -Recurse -Depth 2 -Force |
    Where-Object { $_.Mode -match "h" -and $_.FullName -like "*\.git" } |
        ForEach-Object { cd $_.FullName; cd ../; git pull; cd ../ }
----
