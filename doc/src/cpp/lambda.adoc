= Lambdas

Могут получать значения извне (замыкание).
Тип замыкания и передаваемые значения опеределяются содержанием квадратных
скобок перед лямбдой.

== As function parameter

Declaration:

[source]
----
extern void line(std::function<void(int const &x, int const &y)>const &found_point);
----

Calling:

[source]
----
line([&](int const &x, int const &y) {/*EMPTY*/});

line([&](int const &x, int const &y) -> void {print(x, y);});

line([&](auto &x, auto &y) -> auto {print(x, y);});

line([](auto x, auto y) {print(x, y);});

line([&](auto &x, auto &y) {print(x, y);});
----
