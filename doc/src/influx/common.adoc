= Influx

[source]
----
docker run --rm \
  -e INFLUXDB_DB=hf2 -e INFLUXDB_HTTP_AUTH_ENABLED=true \
  -e INFLUXDB_ADMIN_USER=admin -e INFLUXDB_ADMIN_PASSWORD=admin \
  -e INFLUXDB_USER=user -e INFLUXDB_USER_PASSWORD=user \
  -v $PWD:/var/lib/influxdb influxdb /init-influxdb.sh

docker run --rm --name influxdb -p 8086:8086 -v $PWD:/var/lib/influxdb influxdb
----
