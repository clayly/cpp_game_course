= Service

== Run Node.js server as service

Add `#!/usr/bin/env node` as first line to `app.js`.

Give it `chmod +x /app_folder/app.js`.

Make file `app.service`
* `/etc/systemd/system/` (?)
* `/lib/systemd/system/` (Debian?)

With next content:

[source]
----
[Unit]
Description=My app

[Service]
Restart=always
User=nobody
# Note Debian/Ubuntu uses 'nogroup', RHEL/Fedora uses 'nobody'
Group=nogroup
Environment=PATH=/usr/bin:/usr/local/bin
Environment=NODE_ENV=production
KillSignal=SIGQUIT
WorkingDirectory=/app_folder
ExecStart=/app_folder/app.js

[Install]
WantedBy=multi-user.target
----

Start with: `systemctl start app`.

Enable autostart on boot: `systemctl enable app`.

Last 300 lines of log: `journalctl -u app | tail -n 300`.

Real-time logs: `journalctl -fu app`.
