= Common

== Bash scripts

Running scripts.
[source]
----
source <script>  // run script in context of current shell
source <script>; <function>  // call certain function from script
----

Debug scripts.
[source]
----
// print every command before executing, if its not `echo`
trap '[[ $BASH_COMMAND != echo* ]] && echo $BASH_COMMAND' DEBUG
// print every command with expanded variables using subshells
(set -x; <command0>)
(set -x; <command1>)
bash -x <script> // debug whole script
----

Do something and get back.
[source]
----
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd <dst>
cd $DIR
----

== Useful commands

Get numeric (like 660 or 755) representaion of permissions.
[source]
----
stat -c "%a %n" <path>
----

Copy file permissions etc.
[source]
----
chmod --reference=<src> <dst> // copy mode
chown --reference=<src> <dst> // copy ownership
cp -p{or --preserve} <src> <dst> // may list: -p=mode,ownership, etc.
----

Log out other users.
[source]
----
who -u // get pids
uptime // show logged in users count and system uptime
w // show logged in users
last | head // show last logged in users
ps -ef | grep <user> // get pids
pkill -u <user> // kill all for this username
kill <pid> // kill by pid
----

Run command as super user.
[source]
----
su 0 <command> // if password is not needed, 0 is root's user id on embedded
----

Killing.
[source]
----
kill <pid> // graceful kill
kill -9 <pid> // forceful kill
kill -1 -9 // kill everything you can kill
----

Execute each line of file as command.
[source]
----
while IFS= read -r in; do eval $in; done < <file>
----

Constructs and writes to file <dst> command lines from arguments, taken from file <src>. +
Single quoted 'in'-variable escape in double quoted string in single quoted string.
[source]
----
while IFS= read -r in; do echo 'influx -execute "drop database '$in'"' >> <dst>; done < <src>
----

Find entities, matching any of statement.
[source]
----
find . \( -name *.zip -o -name *.tar.gz \)
----

Check size of entities on depth 1 and sort output size.
[source]
----
du -d 1    . | sort -n  // numeric
du -d 1 -h . | sort -h  // human-readable
----

Recursively run and pull through all dirs. +
`find` by default executes `exec echo {}` on each entity.
[source]
----
find . -name .git -print
    -execdir git config credential.helper store \;
    -execdir git pull \;
----

Recursively run on depth 1 and execute commands on found files.
[source]
----
find $(pwd) -maxdepth 1 -type f -name *.apk
    -exec echo {} \;
    -exec adb install -r -g {} \;
----

Execute command on all entites in curr dir.
[source]
----
for x in *; do adb install -r -g $x; done
----

Zip files to archive with password.
[source]
----
zip -e{encrypt} -r{recursive (for folders)} <out> <src1> <src2>
zip -9 -y -r -q <out> <src> // win-compatible archive
unzip <src> // unzip target
unrar -x <src> // eXtract target
tar -xf <src> -C <dst> // eXtract to folder
----

Lock user or remove user password. +
Result the same: noone will be able to login as this user.
[source]
----
passwd --lock user
passwd -d user
----

Unlock user or set user password.
[source]
----
passwd --unlock user
sudo passwd user
----

Store output to variable.
[source]
----
read <VAR> < <(pwd) // instead of pwd can be full chain of commands)
echo $<VAR> // will print current folder
----

== Background

[source]
----
<cmd> & // run command in background
<cmd> 2> /dev/null & // also redirect output
jobs // list background jobs
jobs -l // list background jobs with their pids
fg %<job_number> // bring job to foreground
kill -9 $(jobs -p) // kill all jobs with fire
----
