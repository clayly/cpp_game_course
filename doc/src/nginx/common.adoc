= Common

== 80-port traffic not reaching Nginx

[source]
----
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
----

== Nginx as systemd-service cant connect to local server

Error from `/var/log/nginx/error.log`: +
`connect() to 127.0.0.1:8080 failed (13: Permission denied) while connecting to upstream`

Solve link: +
https://stackoverflow.com/questions/23948527/13-permission-denied-while-connecting-to-upstreamnginx

Command: +
`setsebool -P httpd_can_network_connect 1`

== Permission denied 13, cant access user's files

=== User access

===== Add to user's group

[source]
----
# each of those
gpasswd -a nginx <user_group>
usermod -aG <user_group> nginx

# check with
id <user>
id nginx
----

===== Provide to user's groups access to target dir

[source]
----
sudo chmod g+x /home \
    && chmod g+x /home/<user>
    && chmod g+x /home/<user>/<target_dir>
----

=== SELinux

Problem is that custom directories have no access for *nginx*.

Instead of `httpd_user_rw_content_t` can be `httpd_sys_content_t`.

[source]
----
# it works alone
sudo chcon -v -R --type=httpd_user_rw_content_t mediafiles

# this double maybe do the same
sudo semanage fcontext -a -t=httpd_user_rw_content_t "/home/cito/running/zwimming/zwiwwing-server-simple/mediafiles(/.*)?"
sudo restorecon -R -v /home/cito/running/zwimming/zwimming-server-simple/mediafiles

# each if this is total disabling, brute force
setsebool -P httpd_disable_trans false
semodule -r httpd
semanage permissive -a httpd_t
----
