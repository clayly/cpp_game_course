#!/bin/bash
source .env

PROJECT=

INSTANCES=(

)

COMMAND=$1
INSTANCE_NUM=$2

if [ "${COMMAND}" == "start" ]; then
    INSTANCE="${INSTANCES[${INSTANCE_NUM}]}"
    echo "command:${COMMAND} project:${PROJECT} instance_num:${INSTANCE_NUM} instance:${INSTANCE}"
    docker-compose \
        --project-name ${PROJECT}_${INSTANCE} \
        --file docker-compose-${INSTANCE}.yml \
        up -d
elif [ "${COMMAND}" == "stop" ]; then
    INSTANCE="${INSTANCES[${INSTANCE_NUM}]}"
    echo "command:${COMMAND} project:${PROJECT} instance_num:${INSTANCE_NUM} instance:${INSTANCE}"
    docker-compose \
        --project-name ${PROJECT}_${INSTANCE} \
        --file docker-compose-${INSTANCE}.yml \
        down
elif [ "${COMMAND}" == "list" ]; then
    echo "command:${COMMAND}"
    for i in "${!INSTANCES[@]}"; do
        INSTANCE="${INSTANCES[${i}]}"
        echo "$i. ${INSTANCE}"
    done
else
    echo "ERROR: UNKNOWN COMMAND"
fi
