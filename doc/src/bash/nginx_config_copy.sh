#!/bin/bash

PROJECTS=(
zwimming/zwimming-devops
hf2/handsfree2devops
runo/runodevops
)

NGINX_CONFIG=/etc/nginx/nginx.conf

for PROJECT in "${PROJECTS[@]}"; do
    cp ${NGINX_CONFIG} "${PROJECT}/nginx/"
done
