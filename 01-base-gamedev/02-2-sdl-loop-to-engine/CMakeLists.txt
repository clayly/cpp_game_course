cmake_minimum_required(VERSION 3.9)
project(sdl_loop_to_engine_proj)

set(EXECUTABLE sdl_loop_to_engine_exec)

add_executable(${EXECUTABLE} main.cxx engine.cxx engine.hxx)
target_compile_features(${EXECUTABLE} PUBLIC cxx_std_17)

find_library(SDL2_LIB NAMES libSDL2.a)

if (MINGW)
    target_link_libraries(${EXECUTABLE}
               -lmingw32 
               -lSDL2main 
               "${SDL2_LIB}"
               -mwindows
               -Wl,--no-undefined
               -lm
               -ldinput8
               -ldxguid
               -ldxerr8
               -luser32 
               -lgdi32
               -lwinmm
               -limm32
               -lole32
               -loleaut32
               -lshell32
               -lversion
               -luuid
               -static-libgcc
               )
elseif(UNIX)
    target_link_libraries(${EXECUTABLE}
               "${SDL2_LIB}"
               -lm
               -ldl
               -lpthread
               -lrt
               )
elseif(MSVC)
    find_package(sdl2 REQUIRED)
    target_link_libraries(${EXECUTABLE} PRIVATE SDL2::SDL2 SDL2::SDL2main)
endif()

if(MSVC)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4 /WX /std:c++17")
else()
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Werror")
endif()

install(TARGETS ${EXECUTABLE}
        RUNTIME DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
        LIBRARY DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests
        ARCHIVE DESTINATION ${CMAKE_CURRENT_LIST_DIR}/../../bin/tests)
