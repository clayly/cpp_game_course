varying vec4 v_position;
void main()
{
    if (v_position.z >= 0.0)
    {
        float light_green = 0.5 + v_position.z / 2.0;
        gl_FragColor = vec4(0.0, light_green, 0.0, 1.0);
    }
    else
    {
        float dark_green = 0.5 - (v_position.z / -2.0);
        gl_FragColor = vec4(0.0, dark_green, 0.0, 1.0);
    }
}