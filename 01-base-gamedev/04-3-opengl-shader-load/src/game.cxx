#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.hxx"

int main(int /*argc*/, char* /*argv*/[])
{
    std::ifstream file("res/base.frag", std::ios::binary | std::ios::ate);
    long fileSize = file.tellg();
    char* const fileArr = new char[fileSize];
    //    file.read(fileArr, fileSize);
    //    for (long i = 0; i < fileSize; ++i) {
    //        std::cout << *(i+fileArr);
    //    }
    //    std::cout << std::endl;
    //    std::unique_ptr<om::engine, void (*)(om::engine*)> engine(
    //            om::create_engine(), om::destroy_engine);
    //
    //    const std::string error = engine->initialize("");
    //    if (!error.empty())
    //    {
    //        std::cerr << error << std::endl;
    //        return EXIT_FAILURE;
    //    }
    //
    //    bool continue_loop = true;
    //    while (continue_loop)
    //    {
    //        om::event event;
    //
    //        while (engine->read_input(event))
    //        {
    //            std::cout << event << std::endl;
    //            switch (event)
    //            {
    //                case om::event::turn_off:
    //                    continue_loop = false;
    //                    break;
    //                default:
    //                    break;
    //            }
    //        }
    //
    //        std::ifstream file("res/vertexes.txt");
    //        assert(!!file);
    //
    //        om::triangle tr;
    //        file >> tr;
    //
    //        engine->render_triangle(tr);
    //
    //        file >> tr;
    //        engine->render_triangle(tr);
    //
    //        engine->swap_buffers();
    //    }
    //
    //    engine->uninitialize();

    delete[] fileArr;
    return EXIT_SUCCESS;
}