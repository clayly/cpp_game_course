#pragma once

#include <cmath>
#include <cstdint>
#include <functional>
#include <iostream>
#include <utility>

namespace bresenham {

struct point2d {
    int x;
    int y;
};

namespace {
    void line_presorted(
        int& major_ptr, int& major_end, int const& major_range,
        int& minor_ptr, int& minor_end, int const& minor_range,
        std::function<void(int const& x, int const& y)> const& callback) noexcept
    {
        int const minor_step = minor_ptr > minor_end ? -1 : 1;
        int const minor_err_x2_incr = minor_range << 1;
        int const minor_err_x2_decr = major_range << 1;
        int minor_err_x2 = 0;
        for (; major_ptr != major_end + 1; ++major_ptr) {
            callback(major_ptr, minor_ptr);
            minor_err_x2 += minor_err_x2_incr;
            if (minor_err_x2 > major_range) {
                minor_ptr += minor_step;
                minor_err_x2 -= minor_err_x2_decr;
            }
        }
    }

    void process_error(
        int& error,
        int& x_range,
        int& y_range,
        int& x_dir,
        int& x) noexcept
    {
        if (x_range > y_range) {
            while (error << 1 < x_range) {
                error += y_range;
                x += x_dir;
            }
            error -= x_range;
        } else {
            error += x_range;
            if (error << 1 >= y_range) {
                x += x_dir;
                error -= y_range;
            }
        }
    }
}

void line(
    point2d const& point0,
    point2d const& point1,
    std::function<void(int const& x, int const& y)> const& callback) noexcept
{
    int x0 = point0.x;
    int y0 = point0.y;
    int x1 = point1.x;
    int y1 = point1.y;
    int y_range = std::abs(y0 - y1);
    int x_range = std::abs(x0 - x1);
    if (x_range < y_range) {
        if (x0 > x1) {
            std::swap(x0, x1);
            std::swap(y0, y1);
        }
        line_presorted(y0, y1, y_range, x0, x1, x_range,
            callback);
    } else {
        if (y0 > y1) {
            std::swap(x0, x1);
            std::swap(y0, y1);
        }
        line_presorted(x0, x1, x_range, y0, y1, y_range,
            [&](auto x, auto y) { callback(y, x); });
    }
}

void triangle_hollow(
    point2d const& point0,
    point2d const& point1,
    point2d const& point2,
    std::function<void(int const& x, int const& y)> const& callback) noexcept
{
    line(point0, point1, callback);
    line(point1, point2, callback);
    line(point2, point0, callback);
}

void triangle_filled(
    point2d const& point0,
    point2d const& point1,
    point2d const& point2,
    std::function<void(int const& x, int const& y)> const& callback) noexcept
{
    point2d p0 = point0;
    point2d p1 = point1;
    point2d p2 = point2;
    if (p0.y > p1.y)
        std::swap(p0, p1);
    if (p0.y > p2.y)
        std::swap(p0, p2);
    if (p1.y > p2.y)
        std::swap(p1, p2);
    int& x0 = p0.x;
    int& x1 = p1.x;
    int& x2 = p2.x;
    int& y0 = p0.y;
    int& y1 = p1.y;
    int& y2 = p2.y;
    int x01_range = std::abs(x0 - x1);
    int x02_range = std::abs(x0 - x2);
    int x12_range = std::abs(x1 - x2);
    int y01_range = y1 - y0;
    int y02_range = y2 - y0;
    int y12_range = y2 - y1;
    int x01_dir = x0 - x1 > 0 ? -1 : 1;
    int x02_dir = x0 - x2 > 0 ? -1 : 1;
    int x12_dir = x1 - x2 > 0 ? -1 : 1;
    callback(x0, y0);
    callback(x1, y1);
    callback(x2, y2);
    std::cout << "x01_dir " << x01_dir << std::endl;
    std::cout << "x02_dir " << x02_dir << std::endl;
    std::cout << "x12_dir " << x12_dir << std::endl;
    int err01 = 0;
    int err02 = 0;
    int err12 = 0;
    bool left2right = (x0 - x1) < 0;
    std::cout << "left2right " << left2right << std::endl;
    int x_from;
    int x_to;
    std::cout << "y0 " << y0 << std::endl;
    std::cout << "y1 " << y1 << std::endl;
    std::cout << "y2 " << y2 << std::endl;
    std::cout << "x0 " << x0 << std::endl;
    std::cout << "x1 " << x1 << std::endl;
    std::cout << "x2 " << x2 << std::endl;
    int x01 = x0;
    int x02 = x0;
    int x12 = x1;
    int y02 = y0;
    for (; y02 <= y1; ++y02) {
        process_error(err02, x02_range, y02_range, x02_dir, x02);
        process_error(err01, x01_range, y01_range, x01_dir, x01);
        x_from = left2right ? x02 : x01;
        x_to = left2right ? x01 : x02;
        std::cout << "y02 " << y02
                  << " x_from " << x_from
                  << " x_to " << x_to
                  << std::endl;
        for (; x_from < x_to; ++x_from) {
            callback(x_from, y02);
        }
    }
    //for (; y02 < y02 + y12_range; ++y02) {
    //    process_error(err02, x02_range, y02_range, x02_dir, x02);
    //    process_error(err12, x12_range, y12_range, x12_dir, x12);
    //    x_from = left2right ? x02 : x12;
    //    x_to = left2right ? x12 : x02;
    //    std::cout << "y02 " << y02
    //              << " x_from " << x_from
    //              << " x_to " << x_to
    //              << std::endl;
    //    for (; x_from < x_to; ++x_from) {
    //        callback(x_from, y02);
    //    }
    //}
}

void circle_hollow(
    point2d const& center,
    int const& radius,
    std::function<void(int const& x, int const& y)> const& callback) noexcept
{
    int const diameter = radius << 1;
    int x_shift = radius - 1;
    int y_shift = 0;
    int delta_x = 1;
    int delta_y = 1;
    int error = delta_x - diameter;
    while (x_shift >= y_shift) {
        callback(center.x + x_shift, center.y + y_shift);
        callback(center.x + y_shift, center.y + x_shift);
        callback(center.x - y_shift, center.y + x_shift);
        callback(center.x - x_shift, center.y + y_shift);
        callback(center.x - x_shift, center.y - y_shift);
        callback(center.x - y_shift, center.y - x_shift);
        callback(center.x + y_shift, center.y - x_shift);
        callback(center.x + x_shift, center.y - y_shift);
        if (error <= 0) {
            ++y_shift;
            error += delta_y;
            delta_y += 2;
        }
        if (error > 0) {
            --x_shift;
            delta_x += 2;
            error += delta_x - diameter;
        }
    }
}
}
