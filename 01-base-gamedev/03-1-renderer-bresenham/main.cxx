#include <array>
#include <cmath>
#include <cstddef>
#include <fstream>
#include <iostream>
#include <limits>
#include <vector>

#include "bresenham.hxx"

<<<<<<< HEAD
void print(
        int const &coor1,
        int const &coor2
) {
=======
void print(int const& coor1, int const& coor2)
{
>>>>>>> 220840a8135fc57b44953b71740e914ac43ffbe2
    std::cout << coor1 << " " << coor2 << std::endl;
}

constexpr size_t width = 320;
constexpr size_t height = 240;

#pragma pack(push, 1)
struct color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};
#pragma pack(pop)

constexpr size_t color_size = sizeof(color);

static_assert(3 == color_size, "24 bit per pixel(r,g,b)");

constexpr size_t buffer_size = width * height;

template <size_t N>
void write_image(
    const std::string& file_name,
    const std::array<color, N>& buffer)
{
    std::ofstream out_file;
    out_file.exceptions(std::ios_base::failbit);
    out_file.open(file_name, std::ios_base::binary);
    out_file << "P6\n"
             << width << ' ' << height << ' ' << 255 << '\n';
    out_file.write(reinterpret_cast<const char*>(&buffer), sizeof(color) * N);
}

void set_pixel(
    int const& x, int const& y,
    std::array<color, buffer_size>& buffer,
    color const& c)
{
    color& col = buffer.at(y * width + x);
    col = c;
}

<<<<<<< HEAD
void operate(
        const int array[],
        size_t array_size,
        int operation(int)
) {
    for (size_t i = 0; i < array_size; i++) {
        std::cout << operation(array[i]) << ' ';
=======
void display(const int a[], size_t n, int operation(int))
{
    for (size_t i = 0; i < n; i++) {
        std::cout << operation(a[i]) << ' ';
>>>>>>> 220840a8135fc57b44953b71740e914ac43ffbe2
    }
    std::cout << std::endl;
}

int main(int argc, char** argv)
{

<<<<<<< HEAD
    const color black = {  0,   0,   0};
    const color white = {255, 255, 255};
    const color green = {  0, 255,   0};
=======
    const color black = { 0, 0, 0 };
    const color white = { 255, 255, 255 };
    const color green = { 0, 255, 0 };
>>>>>>> 220840a8135fc57b44953b71740e914ac43ffbe2

    std::array<color, buffer_size> buffer{};
    buffer.fill(black);

    bresenham::point2d p0 = { 20, 20 };
    bresenham::point2d p1 = { 30, 180 };
    bresenham::point2d p2 = { 180, 30 };
    bresenham::point2d p3 = { 220, 100 };

    bresenham::line(p1, p0, [&](auto const& x, auto const& y) {
        set_pixel(x, y, buffer, white);
    });

    bresenham::line(p0, p2, [&](auto const& x, auto const& y) {
        set_pixel(x, y, buffer, white);
    });

    bresenham::line(p3, p2, [&](auto const& x, auto const& y) {
        set_pixel(x, y, buffer, white);
    });

    bresenham::line(p1, p3, [&](auto const& x, auto const& y) {
        set_pixel(x, y, buffer, white);
    });

    write_image("line.ppm", buffer);
    buffer.fill(black);

    return 0;
}
