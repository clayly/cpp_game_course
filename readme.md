# cpp_game_course

![potato](doc/img/potato.png)

Build Platform   | Status
---------------- | ----------------
Linux x64        | ![Linux x64](https://img.shields.io/bitbucket/pipelines/atlassian/adf-builder-javascript.svg)

The quick brown fox jumps over the lazy dog.

## Docker image

Self-made public one from https://hub.docker.com/r/clayly/cppfedora/.

In previous one (https://hub.docker.com/r/rikorose/gcc-cmake/)
you started from /opt/atlassian/pipelines/agent/build.

## Study plan

1. Correct `HelloWorld` with all checks, check its size.
1. Build minimal size `HelloWorld`.
Maybe with use of assembly and syscall.
1. Build full-static `HelloWorld` with `CMake`.
Use CMake for all future work.
1. Move out `print` function for `HelloWorld` to cxx/hxx.
1. Build `print` sources as static/dynamic library.
1. Build `SDL` from source as static/dynamic library.
Both debug/release version, check all `CMake` options.
Try to use `CMake GUI`.
1. Build `SDL` initialization program.
It most perform comparison of actual version of `SDL`, which is used,
and version of `SDL`, whick was used for this program building.
Debug it with `gdb` and whatever you want.
